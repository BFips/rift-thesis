﻿using UnityEngine;
using System.Collections;

public class ApplyShoot : MonoBehaviour {

    private Rigidbody _rbody;
    private Transform _transform;

    // Use this for initialization
    void Start () {
         _rbody = this.GetComponent<Rigidbody>();
        _transform = this.GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other) {
        if(other.tag == "Foot") {
            Vector3 footPosition = other.transform.position;
            Vector3 ballPositon = _transform.position;
            Vector3 direction = new Vector3(ballPositon.x - footPosition.x, ballPositon.y - footPosition.y, ballPositon.z - footPosition.z);
            _rbody.AddForce(direction * 10, ForceMode.Impulse);
        }
    }
}
