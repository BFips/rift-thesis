﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NetworkPlayer : NetworkBehaviour {

    public Camera playerCamera;

    public override void OnStartLocalPlayer() {
        Animator animator = gameObject.GetComponent<Animator>();
        animator.enabled = true;
    }

    void Start () {
        if (isLocalPlayer) {
            RegistPlayer();
            return;
        }

        playerCamera.enabled = false;

    }


    void RegistPlayer() {
        AvatarController ac = gameObject.GetComponent<AvatarController>();
        if (ac == null) return;

        KinectManager km = KinectManager.Instance;
        if (km == null) {
            Debug.Log("No Kinect Manager");
            return;
        }

        long userId = km.GetUserIdByIndex(ac.playerIndex);
        if (userId != 0) {
            ac.SuccessfulCalibration(userId);
        }

        km.AddAvatarController(ac);
        Debug.Log("Add through regist");
        /*
        // locate the available avatar controllers
        MonoBehaviour[] monoScripts = FindObjectsOfType(typeof(MonoBehaviour)) as MonoBehaviour[];
        km.avatarControllers.Clear();

        foreach (MonoBehaviour monoScript in monoScripts) {
            if ((monoScript is AvatarController) && monoScript.enabled) {
                AvatarController avatar = (AvatarController)monoScript;
                km.avatarControllers.Add(avatar);
            }
        }*/
    }


}
