﻿using UnityEngine;
using System.Collections;

public class FollowHead : MonoBehaviour {

    public Transform target;
    public Vector3 offset = new Vector3(0f, 0f, 0f);

    private void Start() {
       // transform.position 
    }


    private void LateUpdate() {
        float parentRotation = transform.parent.localEulerAngles.y;
        Vector3 newOffset = offset;
        newOffset.x *= Mathf.Sin((parentRotation*Mathf.PI)/180);
        newOffset.z *= Mathf.Cos((parentRotation * Mathf.PI) / 180);
        transform.position = target.position + newOffset;

       
    }
}
